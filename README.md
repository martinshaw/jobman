jobman
=================

This is my hardened, personal use CLI for managing and running scheduled jobs reliably.

Used daily for screenshotting websites and sending payments regularly.

# Usage
<!-- usage -->
```sh-session
$ npm install -g jobman
$ jobman COMMAND
running command...
$ jobman (--version)
jobman/0.0.0 darwin-x64 node-v18.0.0
$ jobman --help [COMMAND]
USAGE
  $ jobman COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`jobman hello PERSON`](#jobman-hello-person)
* [`jobman hello world`](#jobman-hello-world)
* [`jobman help [COMMAND]`](#jobman-help-command)
* [`jobman plugins`](#jobman-plugins)
* [`jobman plugins:install PLUGIN...`](#jobman-pluginsinstall-plugin)
* [`jobman plugins:inspect PLUGIN...`](#jobman-pluginsinspect-plugin)
* [`jobman plugins:install PLUGIN...`](#jobman-pluginsinstall-plugin-1)
* [`jobman plugins:link PLUGIN`](#jobman-pluginslink-plugin)
* [`jobman plugins:uninstall PLUGIN...`](#jobman-pluginsuninstall-plugin)
* [`jobman plugins:uninstall PLUGIN...`](#jobman-pluginsuninstall-plugin-1)
* [`jobman plugins:uninstall PLUGIN...`](#jobman-pluginsuninstall-plugin-2)
* [`jobman plugins update`](#jobman-plugins-update)

## `jobman hello PERSON`

Say hello

```
USAGE
  $ jobman hello [PERSON] -f <value>

ARGUMENTS
  PERSON  Person to say hello to

FLAGS
  -f, --from=<value>  (required) Whom is saying hello

DESCRIPTION
  Say hello

EXAMPLES
  $ jobman hello friend --from martin
  hello friend from martin! (./src/commands/hello/index.ts)
```

_See code: [dist/commands/hello/index.ts](https://gitlab.com/martinshaw/jobman/blob/v0.0.0/dist/commands/hello/index.ts)_

## `jobman hello world`

Say hello world

```
USAGE
  $ jobman hello world

DESCRIPTION
  Say hello world

EXAMPLES
  $ jobman hello world
  hello world! (./src/commands/hello/world.ts)
```

## `jobman help [COMMAND]`

Display help for jobman.

```
USAGE
  $ jobman help [COMMAND] [-n]

ARGUMENTS
  COMMAND  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for jobman.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.1.10/src/commands/help.ts)_

## `jobman plugins`

List installed plugins.

```
USAGE
  $ jobman plugins [--core]

FLAGS
  --core  Show core plugins.

DESCRIPTION
  List installed plugins.

EXAMPLES
  $ jobman plugins
```

_See code: [@oclif/plugin-plugins](https://github.com/oclif/plugin-plugins/blob/v2.0.11/src/commands/plugins/index.ts)_

## `jobman plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ jobman plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.

  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.

ALIASES
  $ jobman plugins add

EXAMPLES
  $ jobman plugins:install myplugin 

  $ jobman plugins:install https://github.com/someuser/someplugin

  $ jobman plugins:install someuser/someplugin
```

## `jobman plugins:inspect PLUGIN...`

Displays installation properties of a plugin.

```
USAGE
  $ jobman plugins:inspect PLUGIN...

ARGUMENTS
  PLUGIN  [default: .] Plugin to inspect.

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Displays installation properties of a plugin.

EXAMPLES
  $ jobman plugins:inspect myplugin
```

## `jobman plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ jobman plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.

  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.

ALIASES
  $ jobman plugins add

EXAMPLES
  $ jobman plugins:install myplugin 

  $ jobman plugins:install https://github.com/someuser/someplugin

  $ jobman plugins:install someuser/someplugin
```

## `jobman plugins:link PLUGIN`

Links a plugin into the CLI for development.

```
USAGE
  $ jobman plugins:link PLUGIN

ARGUMENTS
  PATH  [default: .] path to plugin

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Links a plugin into the CLI for development.

  Installation of a linked plugin will override a user-installed or core plugin.

  e.g. If you have a user-installed or core plugin that has a 'hello' command, installing a linked plugin with a 'hello'
  command will override the user-installed or core plugin implementation. This is useful for development work.

EXAMPLES
  $ jobman plugins:link myplugin
```

## `jobman plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ jobman plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ jobman plugins unlink
  $ jobman plugins remove
```

## `jobman plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ jobman plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ jobman plugins unlink
  $ jobman plugins remove
```

## `jobman plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ jobman plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ jobman plugins unlink
  $ jobman plugins remove
```

## `jobman plugins update`

Update installed plugins.

```
USAGE
  $ jobman plugins update [-h] [-v]

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Update installed plugins.
```
<!-- commandsstop -->

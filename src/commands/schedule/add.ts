import {Command, Flags} from '@oclif/core'
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../../db/models';
import { createSequelizeInstance } from '../../db/db-utils';
import { format, compareAsc, parse, isValid } from 'date-fns'

export default class ScheduleAdd extends Command {
    static description = 'Add job schedule for a job to run at a specified time or repeat at a specified interval';

    static examples = [
        `$ jobman schedule add happy-new-year --date="2023/01/01 00:00:00"`,
        `$ jobman schedule add reminders --interval="hours:1"`,
    ];

    static flags = {
        date: Flags.string({
            char: 'd',
            description: 'Date to run job'
        }),
        interval: Flags.string({
            char: 'i',
            description: 'Interval of time to run schedule. Used when --date is not specified. Format: unit of time separated by number of unit using colon (e.g. hours:1 or minutes:10)'
        }),
    };

    static args = [{name: 'name', description: 'Named used to store job script', required: true}];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(ScheduleAdd);

        const jobScriptName = args.name;

        this.log('Scheduling a new schedule for job ' + jobScriptName + ' ...');

        const jobValue = args.name;
        let scheduleTypeValue: string|null = null;
        const dateValue = flags.date ? flags.date : null;
        const intervalValue = flags.interval ? flags.interval : null;

        if (flags.date) {
            scheduleTypeValue = 'date';

            const dateParsedValue = parse(dateValue as string, 'yyyy/MM/dd HH:mm:ss', new Date());
            if (!isValid(dateParsedValue)) {
                this.error('Invalid date format. Please use format yyyy/MM/dd HH:mm:ss');
                return;
            }
        } else if (flags.interval) {
            scheduleTypeValue = 'interval';

            const intervalParts = (intervalValue as string).split(':');
            if (intervalParts.length !== 2) {
                this.error('Invalid interval format. Please use format unit of time separated by number of unit using colon (e.g. hours:1 or minutes:10)');
                return;
            }

            const intervalUnit = intervalParts[0];
            if (!['hours', 'minutes', 'seconds'].includes(intervalUnit)) {
                this.error('Invalid interval unit. Please use format unit of time separated by number of unit using colon (e.g. hours:1 or minutes:10)');
                return;
            }

            const intervalValueParsed = parseInt(intervalParts[1]);
            if (isNaN(intervalValueParsed)) {
                this.error('Invalid interval value. Please use format unit of time separated by number of unit using colon (e.g. hours:1 or minutes:10)');
                return;
            }

            if (intervalValueParsed < 1) {
                this.error('Invalid interval value. Please use value greater than 0');
                return;
            }

            if (intervalUnit === 'hours') {
                if (intervalValueParsed > 24) {
                    this.error('Invalid interval value. Please use value less than 24');
                    return;
                }
            }

            if (intervalUnit === 'minutes') {
                if (intervalValueParsed > 60) {
                    this.error('Invalid interval value. Please use value less than 60');
                    return;
                }
            }

            if (intervalUnit === 'seconds') {
                if (intervalValueParsed > 60) {
                    this.error('Invalid interval value. Please use value less than 60');
                    return;
                }
            }
        } else {
            this.error('Must specify either --date or --interval');
            return;
        }

        const sequelize = createSequelizeInstance();

        sequelize.sync().then(() => {
            models.Job.findOne({
                where: {
                    name: jobValue
                }
            }).then(job => {
                if (!job) {
                    this.error('Job ' + jobValue + ' does not exist');
                    return;
                }

                const scheduleModelInstance = models.Schedule.build({
                    job: jobValue,
                    type: scheduleTypeValue,
                    date: dateValue,
                    interval: intervalValue,
                });

                this.log('Saving schedule...');

                scheduleModelInstance.save().then(() => {
                    this.log('Saved schedule to database as #' + (scheduleModelInstance as any).id);
                }).catch((err) => {
                    this.error('Failed to save schedule to database: ' + err);
                });
            }).catch((err) => {
                this.error(err);
            });
        }).catch((err) => {
            this.error(err);
        });
    };
}

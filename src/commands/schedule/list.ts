import {Command, Flags} from '@oclif/core';
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../../db/models';
import { createSequelizeInstance } from '../../db/db-utils';

export default class ScheduleList extends Command {
    static description = 'List Schedules';

    static examples = [`$ jobman schedule list`];

    static flags = {};

    static args = [];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(ScheduleList);

        const sequelize = createSequelizeInstance();

        sequelize.sync().then(() => {
            models.Schedule.findAll({})
                .then(schedules => {
                    schedules.forEach(schedule => {
                        this.log(
                            '#' + schedule.getDataValue('id') + "\n" +
                            'Job: ' + schedule.getDataValue('job') + "\n" +
                            'Type: ' + schedule.getDataValue('type') + "\n" +
                            'Date: ' + schedule.getDataValue('date') + "\n" +
                            'Interval: ' + schedule.getDataValue('interval') + "\n" +
                            'Enabled: ' + schedule.getDataValue('enabled') + "\n" 
                        );
                    });
                });
        });
    };
}

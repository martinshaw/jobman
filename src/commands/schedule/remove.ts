import {Command, Flags} from '@oclif/core'
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../../db/models';
import { createSequelizeInstance } from '../../db/db-utils';

export default class ScheduleRemove extends Command {
    static description = 'Remove job schedule';

    static examples = [`$ jobman schedule remove 1`];

    static flags = {};

    static args = [{name: 'id', description: 'Numeric identifier of the schedule', required: true}];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(ScheduleRemove);

        const scheduleId = args.id;

        this.log('Removing schedule #' + scheduleId + ' ...');
        
        const sequelize = createSequelizeInstance();

        sequelize.sync().then(() => {
            models.Schedule.findOne({
                where: {
                    id: scheduleId
                }
            }).then((existingSchedule) => {
                if (!existingSchedule) {
                    return;
                }

                existingSchedule.destroy();

                this.log('Schedule #' + scheduleId + ' removed from DB.');

                return;
            });

            return;
        }).catch((err) => {
            this.error(err);
        });
    };
}

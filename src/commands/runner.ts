import {Command, Flags} from '@oclif/core'
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../db/models';
import { createSequelizeInstance } from '../db/db-utils';
import { Op } from 'sequelize';

export default class Runner extends Command {
    static description = 'Run schedule runner';

    static examples = [`$ jobman runner`];

    static flags = {};

    static args = [];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(Runner);

        const second = 1000;
        const minute = second * 60;
        const hour = minute * 60;
        const timeBeforeRecheck = hour;

        const sequelize = createSequelizeInstance();

        while(true) {
            console.log(1);

            sequelize.sync().then(() => {
                models.Schedule.findAll({
                    where: {
                        type: 'date',
                        date: {
                            [Op.lte]: new Date()
                        },
                        enabled: true
                    }
                }).then((schedules) => {
                    schedules.forEach((schedule) => {
                        const date = new Date(schedule.getDataValue('date'));
                        const now = new Date();

                        if (date.getTime() <= now.getTime()) {
                            this.log('Running schedule #' + schedule.getDataValue('id') + ' ...');

                            const jobName = schedule.getDataValue('job');
                            console.log(123, jobName);

                            models.Job.findOne({
                                where: {
                                    name: jobName
                                }
                            }).then(async (job) => {
                                // @TODO - on multiple failing runs, some bizarre DB error occurs here 
                                /**
                                 * /Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/dialects/sqlite/query.js:227
      const errForStack = new Error();
                          ^
Error
    at Database.<anonymous> (/Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/dialects/sqlite/query.js:227:27)
    at /Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/dialects/sqlite/query.js:225:50
    at new Promise (<anonymous>)
    at Query.run (/Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/dialects/sqlite/query.js:225:12)
    at /Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/sequelize.js:640:28
    at async SQLiteQueryInterface.changeColumn (/Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/dialects/sqlite/query-interface.js:48:40)
    at async Function.sync (/Users/martinshaw-maltamac/Sites/jobman/node_modules/sequelize/src/model.js:1396:11) {
  name: 'SequelizeUniqueConstraintError',
  errors: [
    ValidationErrorItem {
      message: 'id must be unique',
      type: 'unique violation',
      path: 'id',
      value: null,
      origin: 'DB',
      instance: null,
      validatorKey: 'not_unique',
      validatorName: null,
      validatorArgs: []
    }
  ],
  parent: [Error: SQLITE_CONSTRAINT: UNIQUE constraint failed: jobs_backup.id] {
    errno: 19,
    code: 'SQLITE_CONSTRAINT',
    sql: 'INSERT INTO `jobs_backup` SELECT `id`, `name`, `script`, `enabled`, `lastRun`, `createdAt`, `updatedAt` FROM `jobs`;'
  },
                                 */

                                console.log(234, job);

                                if (!job) {
                                    this.log('Job not found');
                                    return;
                                }

                                const scriptPath = job.getDataValue('script');

                                // @todo - This needs to be fixed. See https://javascript.info/modules-dynamic-imports for a lead
                                let jobFunction = await import(scriptPath);

                                // jobFunction = jobFunction.default;

                                jobFunction({
                                    artifactsPath: '/',
                                    job: job,
                                    log: (message: string) => {
                                        this.log(message);
                                    }
                                });
                            });
                        }
                    });
                });
            });

                        //     const job = schedule.job;

                        //     const script = path.join(process.cwd(), job.script);

                        //     const scriptContent = fs.readFileSync(script, 'utf8');

                        //     const scriptExecution = new Function(scriptContent);

                        //     scriptExecution();

                        //     schedule.lastRun = now;

                        //     schedule.save();

                        //     this.log('Schedule #' + schedule.id + ' run.');
                  

            // Sleep for a period
            await new Promise((resolve) => {
                setTimeout(resolve, timeBeforeRecheck);
            });
        }
    };
}

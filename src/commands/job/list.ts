import {Command, Flags} from '@oclif/core';
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../../db/models';
import { createSequelizeInstance } from '../../db/db-utils';

export default class JobList extends Command {
    static description = 'List jobs';

    static examples = [`$ jobman job list`];

    static flags = {};

    static args = [];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(JobList);

        const sequelize = createSequelizeInstance();

        sequelize.sync().then(() => {
            models.Job.findAll({})
                .then(jobs => {
                    jobs.forEach(job => {
                        
                        this.log(
                            '#' + job.getDataValue('id') + "\n" +
                            'Name: ' + job.getDataValue('name') + "\n" +
                            'Script: ' + job.getDataValue('script') + "\n" +
                            'Enabled: ' + job.getDataValue('enabled') + "\n" +
                            'Last Run: ' + job.getDataValue('lastRun') + "\n" 
                        );
                    });
                });
        });
    };
}

import {Command, Flags} from '@oclif/core'
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../../db/models';
import { createSequelizeInstance } from '../../db/db-utils';

export default class JobAdd extends Command {
    static description = 'Add / link job script for scheduling later';

    static examples = [`$ jobman job add examplejob.js`];

    static flags = {};

    static args = [{name: 'path', description: 'Relative path to job script file, must be .js file', required: true}];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(JobAdd);

        const relativePathToJobScript = path.basename(args.path);
        const absolutePathToJobScript = path.resolve(process.cwd(), relativePathToJobScript);
        const jobScriptExtension = path.extname(args.path);
        const jobScriptName = path.basename(relativePathToJobScript, jobScriptExtension);

        if (!fs.existsSync(absolutePathToJobScript)) {
            this.error('Invalid file path')
            return;
        }

        if (jobScriptExtension !== '.js') {
            this.error('File must be a javascript file ending with .js')
            return;
        }

        this.log('Adding job script ' + relativePathToJobScript + ' as ' + jobScriptName + ' ...');

        // Prepare linking of job scripts

        const projectRoot = path.resolve(__dirname, '../../../');
        const jobScriptsPath = path.resolve(projectRoot, 'jobs');
        
        const localLinkPath = path.resolve(jobScriptsPath, jobScriptName + jobScriptExtension);
        const remoteLinkPath = absolutePathToJobScript;

        if (fs.existsSync(localLinkPath)) {
            this.error(`Job script has already been linked into this instance of jobman.\n Use 'jobman job remove ${jobScriptName + jobScriptExtension}' to unlink and remove it.`);
            return;
        }

        // Prepare creation of Job model instance in DB
        const sequelize = createSequelizeInstance();

        sequelize.sync().then(() => {
            models.Job.findAll({
                where: {
                    name: jobScriptName
                }
            }).then((existingJobs) => {
                if (existingJobs.length > 0) {
                    this.error(`Job script with name '${jobScriptName}' already exists in the DB of this instance of jobman.\n Use 'jobman job remove ${jobScriptName + jobScriptExtension}' to remove it.`);
                    return;
                }

                models.Job.findAll({
                    where: {
                        script: localLinkPath
                    }
                }).then((existingJobs) => {
                    if (existingJobs.length > 0) {
                        this.error(`Job script with script path '${localLinkPath}' already exists in the DB of this instance of jobman.\n Use 'jobman job remove ${jobScriptName + jobScriptExtension}' to remove it.`);
                        return;
                    }

                    const jobModelInstance = models.Job.build({
                        name: jobScriptName,
                        script: localLinkPath,
                        enabled: true,
                        lastRun: null,
                    });

                    this.log('Saving job...');

                    jobModelInstance.save().then(() => {
                        this.log('Saved job to database as #' + (jobModelInstance as any).id + ' ' + (jobModelInstance as any).name);

                        this.log('Linking job script...');

                        fs.link(remoteLinkPath, localLinkPath, (err) => {
                            if (err) {
                                this.error(err);
                                return;
                            }

                            this.log('Linked job script to ' + localLinkPath);
                        });
                    }).catch((err) => {
                        this.error('Failed to save job to database: ' + err);
                    });
                });

                return;
            });

            return;
        }).catch((err) => {
            this.error(err);
        });
    };
}

import {Command, Flags} from '@oclif/core'
import * as process from 'process';
import * as fs from 'fs';
import * as path from 'path';
import * as models from '../../db/models';
import { createSequelizeInstance } from '../../db/db-utils';

export default class JobRemove extends Command {
    static description = 'Remove / unlink job script';

    static examples = [`$ jobman job remove examplejob`];

    static flags = {};

    static args = [{name: 'name', description: 'Named used to store job script', required: true}];

    async run(): Promise<void> {
        const {args, flags} = await this.parse(JobRemove);

        const jobScriptName = args.name;

        this.log('Removing job script ' + jobScriptName + ' ...');
        
        const sequelize = createSequelizeInstance();

        sequelize.sync().then(() => {
            models.Job.findOne({
                where: {
                    name: jobScriptName
                }
            }).then((existingJob) => {
                if (!existingJob) {
                    return;
                }

                // @todo: remove associated schedules

                const remoteLinkPath = existingJob.getDataValue('script');
                existingJob.destroy();

                if (fs.existsSync(remoteLinkPath)) {
                    fs.unlinkSync(remoteLinkPath);
                }

                this.log('Job script ' + jobScriptName + ' removed from DB.');
                this.log('Job script ' + jobScriptName + ' unlinked from this instance of jobman.');

                return;
            });

            return;
        }).catch((err) => {
            this.error(err);
        });
    };
}

import { initDB } from './db/db-utils'
initDB();

export {run} from '@oclif/core'
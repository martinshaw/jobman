import { Sequelize, DataTypes, Model } from 'sequelize';
import { createSequelizeInstance } from '../db-utils';

class Job extends Model {}

Job.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    script: {
        type: DataTypes.STRING,
        allowNull: false
    },
    enabled: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    lastRun: {
        type: DataTypes.DATE,
        allowNull: true
    },
}, {
    sequelize: createSequelizeInstance(),
    modelName: 'Job',
    timestamps: true,
    tableName: 'jobs'
});

Job.sync({ alter: true });

export default Job;
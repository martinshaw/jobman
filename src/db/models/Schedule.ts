import { Sequelize, DataTypes, Model } from 'sequelize';
import { createSequelizeInstance } from '../db-utils';

class Schedule extends Model {}

Schedule.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    job: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    type: {
        type: DataTypes.ENUM('date', 'interval'),
        allowNull: false,
        defaultValue: 'date',
    },
    date: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    interval: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    enabled: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: true,
    },
}, {
    sequelize: createSequelizeInstance(),
    modelName: 'Schedule',
    timestamps: true,
    tableName: 'schedules',
});

Schedule.sync({ alter: true });

export default Schedule;
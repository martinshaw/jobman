import * as path from 'path';
import { Sequelize } from 'sequelize';

export const createSequelizeInstance = (): Sequelize => {
    const projectRoot = path.resolve(__dirname, '../../');
    const dbPath = path.resolve(projectRoot, 'jobman.sqlite');
    
    return new Sequelize({
        dialect: 'sqlite',
        storage: dbPath,
        logging: false,
    });
};

export const initDB = async () => {
    const sequelize = createSequelizeInstance();

    try {
        await sequelize.authenticate();
        const created = sequelize.sync({force: true});
    } catch (error) {
        console.error('Unable to connect to the database: ', error);
    }
} 